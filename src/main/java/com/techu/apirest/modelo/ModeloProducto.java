package com.techu.apirest.modelo;

import java.util.List;
import java.util.Objects;

public class ModeloProducto {

    private long id;
    private String descripcion;
    private double precio;
    private List<ModeloUsuario> usuarios;

    public ModeloProducto() {
    }

    public ModeloProducto(long id, String descripcion, double precio) {
        this.id = id;
        this.descripcion = descripcion;
        this.precio = precio;
    }

    public ModeloProducto(long id, String descripcion, double precio, List<ModeloUsuario> usuarios) {
        this.id = id;
        this.descripcion = descripcion;
        this.precio = precio;
        this.usuarios = usuarios;
    }

    public long getId() {
        return id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public double getPrecio() {
        return precio;
    }

    public List<ModeloUsuario> getUsuarios() {
        return usuarios;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public void setUsuarios(List<ModeloUsuario> usuarios) {
        this.usuarios = usuarios;
    }

    @Override
    public String toString() {
        return "ModeloProducto{" +
                "id=" + id +
                ", descripcion='" + descripcion + '\'' +
                ", precio=" + precio +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ModeloProducto producto = (ModeloProducto) o;
        return getId() == producto.getId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }
}
