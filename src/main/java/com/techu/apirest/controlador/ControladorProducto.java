package com.techu.apirest.controlador;


import com.techu.apirest.modelo.ModeloProducto;
import com.techu.apirest.modelo.ProductoPrecioOnly;
import com.techu.apirest.servicio.ServicioProducto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/apirest/v1")
public class ControladorProducto {

    @Autowired
    private ServicioProducto servicioProducto;

    //Bienvenida en uri raiz
    @GetMapping("")
    public String raiz() {
        return "¡Bienvenidos al Entregable 1!";
    }

    //Obtener todos los productos
    @GetMapping("/productos")
    public List<ModeloProducto> getProductos() {
        return servicioProducto.getProductos();
    }

    //Obtener tamaño de productos
    @GetMapping("/productos/size")
    public int contarProductos() {
        return servicioProducto.getProductos().size();
    }

    //Obtener un solo producto
    @GetMapping("/productos/{id}")
    public ResponseEntity obtenerProductoId(@PathVariable int id) {
        ModeloProducto prod = servicioProducto.getProducto(id);
        if (prod == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(prod);
    }

    //POST
    @PostMapping("/productos")
    public ResponseEntity<String> agregarProducto(@RequestBody ModeloProducto modeloProducto) {
        servicioProducto.agregarProducto(modeloProducto);
        return new ResponseEntity<>("¡Producto creado exitosamente!", HttpStatus.CREATED);
    }

    //PUT
    @PutMapping("/productos/{id}")
    public ResponseEntity actualizarProducto(@PathVariable int id,
                                             @RequestBody ModeloProducto productoActualizado) {
        ModeloProducto prod = servicioProducto.getProducto(id);
        if (prod == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        servicioProducto.actualizarProducto(id-1, productoActualizado);
        return new ResponseEntity<>("Producto actualizado correctamente", HttpStatus.OK);
    }

    //DELETE
    @DeleteMapping("/productos/{id}")
    public ResponseEntity borrarProducto(@PathVariable Integer id) {
        ModeloProducto prod = servicioProducto.getProducto(id);
        if(prod == null) {
            return new ResponseEntity<>("Producto no encontrado", HttpStatus.NOT_FOUND);
        }
        servicioProducto.borrarProducto(id - 1);
        return new ResponseEntity<>("Producto eliminado correctamente.", HttpStatus.OK);
    }

    //PATCH
    @PatchMapping("/productos/{id}")
    public ResponseEntity patchPrecioProducto(@RequestBody ProductoPrecioOnly productoPrecioOnly,
                                              @PathVariable int id) {
        ModeloProducto prod = servicioProducto.getProducto(id);
        if (prod == null){
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        prod.setPrecio(productoPrecioOnly.getPrecio());
        servicioProducto.actualizarProducto(id - 1, prod);
        return new ResponseEntity<>(prod, HttpStatus.OK);
    }

    //GET Subrecurso
    @GetMapping("/productos/{id}/usuarios")
    public ResponseEntity obtenerProductoIdUsuarios(@PathVariable int id) {
        ModeloProducto prod = servicioProducto.getProducto(id);
        if (prod == null) {
            return new ResponseEntity<>("Producto no encontrado.", HttpStatus.NOT_FOUND);
        }
        if (prod.getUsuarios() != null) {
            return ResponseEntity.ok(prod.getUsuarios());
        } else {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

}
