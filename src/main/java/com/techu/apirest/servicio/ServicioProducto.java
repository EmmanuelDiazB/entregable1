package com.techu.apirest.servicio;

import com.fasterxml.jackson.databind.ser.impl.IndexedStringListSerializer;
import com.techu.apirest.modelo.ModeloProducto;
import com.techu.apirest.modelo.ModeloUsuario;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

@Component
public class ServicioProducto {

    private List<ModeloProducto> listaDatos = new ArrayList<ModeloProducto>();

    public ServicioProducto() {
        listaDatos.add(new ModeloProducto(1, "Tenis", 150.00));
        listaDatos.add(new ModeloProducto(2, "Playera", 200.00));
        listaDatos.add(new ModeloProducto(3, "Pantalón", 450.00));
        listaDatos.add(new ModeloProducto(4, "Computadora", 15000.00));
        listaDatos.add(new ModeloProducto(5, "Silla", 300.00));
        List<ModeloUsuario> usuarios = new ArrayList<>();
        usuarios.add(new ModeloUsuario("1"));
        usuarios.add(new ModeloUsuario("5"));
        usuarios.add(new ModeloUsuario("7"));
        listaDatos.get(1).setUsuarios(usuarios);
    }

    //READ
    public List<ModeloProducto> getProductos() {
        return listaDatos;
    }

    //READ un producto
    public ModeloProducto getProducto(long indice) throws IndexOutOfBoundsException {
        if(getIndice(indice) >= 0){
            return listaDatos.get(getIndice(indice));
        }
        return null;
    }

    //CREATE
    public ModeloProducto agregarProducto(ModeloProducto nuevoProd) {
        listaDatos.add(nuevoProd);
        return nuevoProd;
    }

    //UPDATE
    public ModeloProducto actualizarProducto(int indice, ModeloProducto nuevoProd)
            throws IndexOutOfBoundsException {
        listaDatos.set(indice, nuevoProd);
        return listaDatos.get(indice);
    }

    //DELETE
    public void borrarProducto(int indice) throws IndexOutOfBoundsException {
        int pos = listaDatos.indexOf(listaDatos.get(indice));
        listaDatos.remove(pos);
    }

    //Tomar el indice
    public int getIndice(long indice) throws IndexOutOfBoundsException {
        int i = 0;
        while(i < listaDatos.size()) {
            if(listaDatos.get(i).getId() == indice){
                return i;
            }
            i++;
        }
        return -1;
    }

}
